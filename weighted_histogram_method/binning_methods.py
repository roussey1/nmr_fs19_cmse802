from  weighted_histogram_method.wepy_files.hdf5 import WepyHDF5
import mdtraj as mdj
import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from math import pi

class HummerSzaboBinningMethods(object):
    """ This bins probabilities and work values from simulation via equations
        7 or 8 in Hummer & Szabo, PNAS, 2001, 98,7."""
    def __init__(self, T=None, k=None, box=None, d0_start=None, d0_incr=None):

        self.T = T
        self.k = k #300 #K
        self.Kb = 0.008314 #(kj/mol)/K
        self.box = box
        self.d0_start = d0_start
        self.d0_incr = d0_incr
        self.beta = 1/(self.T*self.Kb)

    def min3(self, a,b,c):
        """ This function is only called within the fe_calc functions.
            It is used to determine the minimum of 3 values relating to
            positions.
            Args: 
                a: The interatomic distance.
                b: The interatomic distance - the box length.
                c: The interatomic distance + the box length.
            Returns:
                The minimum of the 3 values.
        """

        if a < b:
            if a < c:
                return a
            else:
                return c
        else:
            if b < c:
                return b
            else:
                return c

    def vec_dist(self, d1,d2,w,j):
        """ This gets the interatomic distance based on positions. 
            Periodic boundaries considered.
            
        Args:
            d1: Position of atom 1 from the HDF5.
            d2: Position of atom 2 from the HDF5.
            w: Walker index.
            j: Cycle index.
        Returns:
            The interatomic distance."""
        tmp = d1-d2
        for i,t in enumerate(tmp):
            tmp[i] = self.min3(abs(t),abs(t-self.box),abs(t+self.box))

        if np.sqrt(np.sum(np.square(tmp))) > 8:
            print("walker",w,"index",j)
            print("d1 is",d1)
            print("d2 is",d2)

        return np.sqrt(np.sum(np.square(tmp)))

    def get_bias_value(self, d, cycle_idx, k):
        """This calcuates the instantaneous bias for a given bin 
        of space.
        Args:
            d(int): The interatomic distance.
            cycle_idx(int): The cycle index.
            k(int): The spring constant used in the simulation (kcal/mol).
        Returns:
            harmonic bias: The bias from a harmonic force at a given point.
        """
        d0 = self.d0_start + cycle_idx*self.d0_incr
        return 0.5*k*(d-d0)**2

    # This is a slow method of creating a free energy surface
    # This is slowbut highly accurate
    def fe_calc_AM(self, wepy_path, n_bins, file_idx):
         
        """         
        This calculates G0 in eq [8] from Hummer, Szabo PNAS 2001 98, 7
        This is a slower, but highly accurate binning method.

        G0(z) =  -1/beta ln[ (sum_t (term1 / term2)) / ( sum_t (term3 / term2)) ]
        
        where
        
        term1 =  <delta(z-z_t) exp(-beta w_t) >     # specific to z
        term2 = <exp(-beta w_t)>
        term3 = exp[-beta u(z,t)]                   # specific to z

        Args:
            wepy_path: An HDF5 file with simulaiton output.
            n_bins(int): The number of bins to use for binning.
            file_idx(int): The file index.
        Returns:
            g0_arr: An array of free energies.
            d_arr: An array of interatomic distances
        """
    

        wepy_h5 = WepyHDF5(wepy_path, mode='r')
        wepy_h5.open()

        run_idx = 0
        n_cycles = int(wepy_h5.run_n_cycles(0))
        n_walkers = int(wepy_h5.resampler_grp(0)['n_walkers'][-1])

        # times is cycles
        times = np.array(list(range(1,n_cycles+1)))

        g0 = np.zeros((n_bins))
        n_g0 = np.zeros((n_bins))
        d_min = 0
        d_max = 8
        d_values = [(i+0.5)*(d_max-d_min)/n_bins for i in range(n_bins)]

        # NUMERATOR
        # for every cycle, do the binning and averaging for all the walkers and sum up for all times

        # This calculates G0 in eq [8] from Hummer, Szabo PNAS 2001 98, 7
        #
        # G0(z) =  -1/beta ln[ (sum_t (term1 / term2)) / ( sum_t (term3 / term2)) ]
        #
        # where
        #
        # term1 =  <delta(z-z_t) exp(-beta w_t) >     # specific to z
        # term2 = <exp(-beta w_t)>
        # term3 = exp[-beta u(z,t)]                   # specific to z
        #

        numer = np.zeros((n_bins))
        denom = np.zeros((n_bins))

        for cycle in range(n_cycles):
            # these lists have all the distances, work values, and weights for cycle i
            ds_cyc = []
            work_cyc = []
            weight_cyc = []

            # initialize variables
            term1 = np.zeros((n_bins))
            term2 = 0
            norm = 0

            for j in range(n_walkers):
                # get distances
                positions = wepy_h5.h5['runs/0/trajectories/'+str(j)+'/positions'][cycle]
                tmp = self.vec_dist(positions[0], positions[1], j, cycle)
                ds_cyc.append(tmp)

                # get intermediate work values
                work_value = wepy_h5.h5['runs/0/trajectories/'+str(j)+'/activity'][cycle][0][0]
                work_cyc.append(work_value)

                # get weights
                weights_val = wepy_h5.h5['runs/0/trajectories/'+str(j)+'/weights'][cycle][0]
                weight_cyc.append(weights_val)

            e_mbwt = np.exp(-self.beta*np.array(work_cyc))

            for j,d in enumerate(ds_cyc):
                # find out which bin it's in
                 bin_id = int((d - d_min)/(d_max - d_min)*n_bins)

                 term1[bin_id] += weight_cyc[j]*e_mbwt[j]
                 term2 += weight_cyc[j]*e_mbwt[j]
                 norm += weight_cyc[j]

            # end of loop over cycles
            # terms have been computed, add to the running sums over timepoints
            for b in range(n_bins):
                numer[b] += term1[b]/term2

                # Note: get_bias_value returns term3
                # need to use cycle+1 so the d0 matches the work values
                term3 = np.exp(-self.beta*self.get_bias_value(d_values[b],cycle+1,self.k))
                denom[b] += term3/(term2/norm)

        g0_no_gaps = []
        d_values_no_gaps = []
        for b in range(n_bins):
            if numer[b] > 0 and denom[b] > 0:
                g0_no_gaps.append(-np.log(numer[b]/denom[b])/self.beta)
                d_values_no_gaps.append(d_values[b])

        g0_arr = np.array(g0_no_gaps)
        d_val_arr = np.array(d_values_no_gaps)

        plt.plot(d_values_no_gaps,g0_arr-g0_arr.min(),label=f'Run {file_idx}, AM')

        #print(f'Done with file {file_idx}')
        return g0_arr, d_val_arr

    # This is the fast method of creating a free energy surface
    # Fast, but inaccurate
    def fe_calc_FM(self, wepy_path, n_bins, file_idx):
                 
        """         
        This calculates G0 in eq [7] from Hummer, Szabo PNAS 2001 98, 7
        This is a fast, but less accurate binning method.
        G0(z) =  -1/beta ln[ (sum_t (term1))] 
        
        where
        
        term1 =  <delta(z-z_t) exp(-beta w_t) >     # specific to z

        Args:
            wepy_path: An HDF5 file with simulaiton output.
            n_bins(int): The number of bins to use for binning.
            file_idx(int): The file index.
        Returns:
            g0_arr: An array of free energies.
            d_arr: An array of interatomic distances.
            
"""

        wepy_h5 = WepyHDF5(wepy_path, mode='r')
        wepy_h5.open()

        run_idx = 0
        n_walkers = int(wepy_h5.resampler_grp(0)['n_walkers'][-1])

        n_cycles = int(wepy_h5.run_n_cycles(0))
        times = np.array(list(range(1,n_cycles+1)))

        g0 = np.zeros((n_bins))
        n_g0 = np.zeros((n_bins))
        g0_min = 0
        g0_max = 8
        d_values = [(i+0.5)*(g0_max-g0_min)/n_bins for i in range(n_bins)]

        # This calculates G0 in eq [7] from Hummer, Szabo PNAS 2001 98, 7

        for i in range(n_walkers):
            # get distances
            ds = np.array([self.vec_dist(d[0],d[1],i,j) for j,d in enumerate(wepy_h5.h5['runs/0/trajectories/'+str(i)+'/positions'])])

            # get intermediate work values
            work_values = np.array([w[0][0] for w in wepy_h5.h5['runs/0/trajectories/'+str(i)+'/activity']])

            # get instantaneous potential energies (of bias)
            inst = np.array([self.get_bias_value(ds[cycle],cycle+1,self.k) for cycle in range(len(ds))])

            # get e**-beta(delta w) values
            e_mb_delta_w = np.exp(-self.beta*(work_values - inst))

            for j,d in enumerate(ds):
                bin_id = int((d - g0_min)/(g0_max - g0_min)*n_bins)

                g0[bin_id] += e_mb_delta_w[j]
                n_g0[bin_id] += 1

        g0_no_gaps = []
        d_values_no_gaps = []
        for i in range(len(g0)):
            if n_g0[i] > 0:
                g0[i] /= n_g0[i]
                g0_no_gaps.append(-np.log(g0[i])/self.beta)
                d_values_no_gaps.append(d_values[i])

        g0_arr = np.array(g0_no_gaps)
        d_val_arr = np.array(d_values_no_gaps)
        plt.plot(d_values_no_gaps,g0_arr-g0_arr.min(),label=f'Run {file_idx}, FM')

        return g0_arr, d_val_arr
