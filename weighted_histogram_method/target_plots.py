from weighted_histogram_method.wepy_files.hdf5 import WepyHDF5
import mdtraj as mdj
import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from math import pi

# make target plot                                                                          
class TargetPlotGenerator(object):
    """ This class generates a target free energy surface for a 
        Lennard-Jones pair with the given variables. These vairiables 
        are temperature (K), epsilon (kcal/mol), sigma (nm), and 
        r (points between the initial and final values used in the pulling
        simualtion that represent interatomic distances). """
    def __init__(self, T=None, epsilon=None, sigma=0.335, r=None):
       

        """
        A set of functions that generates a target free energy plot for a 
        Lennard-Jones pair.
        
        Args:
            T (int): Temperature
            epsilon (int): the value of epsilon used in the simualtion
            sigma (float): the value of sigma used in the simualtion
            r (float): A numpy array of floats, points to calculate the
                free energy surface
        
        Returns:
            numpy array: An array of free energies generated from r.
                with the given inputs
        """

        self.T = T #K
        self.epsilon = epsilon #kcal/mol
        self.r = r 
        self.Kb = 0.008314 #(kJ/mol)/K  
        self.beta = 1/(self.T*self.Kb)
        self.sigma = sigma

    def VLJ(self, r, epsilon):

        """ A function that calculates the Lennard-Jones Potential for a
        given r, sigma, and epsilon.
        
        Args: 
            r: numpy array
            epsilon: int, the value of epsilon used in the simulation being analyzed.
        Returns:
            A number equal to the Lennard-Jones Potential for a given r.
        """

        # sigma is in nanometers
        # epsilon is in kcals initally
        epsilon_kj = self.epsilon*4.184
        sigma_o_r = self.sigma/self.r
        sigma_o_r6 = (sigma_o_r)**6

        return 4*epsilon_kj*(sigma_o_r6*(sigma_o_r6-1))

    def Fanalytic(self):
        
        """ A function that calculates the value to be plotted as the free energy.
        Args:
            None
        Returns: 
            The value to be plotted for the free energy surface for a given r.
        """

        return -(2*np.log(self.r)-self.beta*self.VLJ(self.r, self.epsilon))/self.beta



