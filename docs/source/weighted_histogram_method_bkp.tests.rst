weighted\_histogram\_method\_bkp.tests package
==============================================

Submodules
----------

weighted\_histogram\_method\_bkp.tests.conftest module
------------------------------------------------------

.. automodule:: weighted_histogram_method_bkp.tests.conftest
   :members:
   :undoc-members:
   :show-inheritance:

weighted\_histogram\_method\_bkp.tests.test\_examples module
------------------------------------------------------------

.. automodule:: weighted_histogram_method_bkp.tests.test_examples
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: weighted_histogram_method_bkp.tests
   :members:
   :undoc-members:
   :show-inheritance:
