weighted\_histogram\_method\_bkp package
========================================

Subpackages
-----------

.. toctree::

   weighted_histogram_method_bkp.tests

Submodules
----------

weighted\_histogram\_method\_bkp.binning\_methods module
--------------------------------------------------------

.. automodule:: weighted_histogram_method_bkp.binning_methods
   :members:
   :undoc-members:
   :show-inheritance:

weighted\_histogram\_method\_bkp.target\_plots module
-----------------------------------------------------

.. automodule:: weighted_histogram_method_bkp.target_plots
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: weighted_histogram_method_bkp
   :members:
   :undoc-members:
   :show-inheritance:
