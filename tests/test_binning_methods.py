from weighted_histogram_method.binning_methods import *
import numpy as np

def test_binning_methods_AM_good():
    
    # Test to determine if the AM binning method gives a free energy for
    # every bin created
    run1 = HummerSzaboBinningMethods(T=300, k=2000, box=4, d0_start=0.32, d0_incr=0.032)
    
    ga, da = run1.fe_calc_AM('../sample_files/example1.wepy.h5', 500, file_idx=1)

    assert len(ga) == len(da), "Different number of bins and energies returned"

def test_binning_methods_FM_good():
        
    # Test to determine if the FM binning method gives a free energy for
    # every bin created
    run1 = HummerSzaboBinningMethods(T=300, k=2000, box=4, d0_start=0.32, d0_incr=0.032)
    
    ga, da = run1.fe_calc_FM('../sample_files/example1.wepy.h5', 500, file_idx=1)

    assert len(ga) == len(da), "Different number of bins and energies returned"

    

