from weighted_histogram_method.target_plots import *
import numpy as np

def test_target_plots_good():
    
    # Test to determine if the target plot generator class gives the correct
    # number of points for plotting
    
    r = np.linspace(0.3,2,500)
    target_points = TargetPlotGenerator(T=300, epsilon=10, r=r)
    target = target_points.Fanalytic()
    target -= target.min()
    
    assert len(target) == len(r), "Wrong number of target values made"

# def test_target_plots_bad():
    
#     # Test to determine if the target plot generator class gives the correct
#     # number of points for plotting
    
#     r = np.linspace(0.3,2,500)
#     target_points = TargetPlotGenerator(T=300, epsilon=10, r=r)
#     target = target_points.Fanalytic()
#     target -= target.min()
    
#     assert len(target) != len(r), "Correct number of points made"
    

