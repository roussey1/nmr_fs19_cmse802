=======
Credits
=======

Maintainer
----------

* Nicole Roussey <roussey1@msu.edu>

Contributors
------------

None yet. Why not be the first? See: CONTRIBUTING.rst
