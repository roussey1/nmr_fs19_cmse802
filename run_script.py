""" This file shows how to use the software in weighted_histogram_method.
    Analysis is done for the 2 example files in sample_files and averaged 
    over. Then, two plots are made for the different analysis methods done.
    To use, run "python run_script.py".
"""

# import the necessary packages
import numpy as np
from weighted_histogram_method.binning_methods import HummerSzaboBinningMethods
from weighted_histogram_method.target_plots import TargetPlotGenerator
import matplotlib.pyplot as plt

# make the target plot with 500 bins
r = np.linspace(0.3,2,500) # 500 points between 0.3nm and 2nm
target_points = TargetPlotGenerator(T=300, epsilon=10, r=r) # make the target plot
target = target_points.Fanalytic()
target -= target.min() # fit the plot
print('Done with target')

# analyze the example files
# make the objects for the class
run1 = HummerSzaboBinningMethods(T=300, k=2000, box=4, d0_start=0.32, d0_incr=0.032)
run2 = HummerSzaboBinningMethods(T=300, k=2000, box=4, d0_start=0.32, d0_incr=0.032)

## run with the "AM", or accurate method
# Get results for the highly accurate binning methods (AM)
all_d_val = []
all_g0_arr = []

ga, da = run1.fe_calc_AM('sample_files/example1.wepy.h5', 500, file_idx=1)
all_d_val.append(da)
all_g0_arr.append(ga)

ga2, da2 = run2.fe_calc_AM('sample_files/example2.wepy.h5', 500, file_idx=2)
all_d_val.append(da2)
all_g0_arr.append(ga2)

# Avg over both sets of data for the AM
mins = []
for i in all_d_val:
    mins.append(len(i))
min_v = np.amin(mins)

d_short = []
for i,v in enumerate(all_d_val):
    d_short.append(all_d_val[i][0:min_v])

g0_short = []
for i,v in enumerate(all_g0_arr):
    g0_short.append(all_g0_arr[i][0:min_v])
    
d_mean = np.mean(d_short, axis=0)
g0_mean = np.mean(g0_short, axis=0)
print('Done with long method')

## run with the "FM", or fast method
# Get results for the less accurate, fast binning methods (FM)
all_d_FM = []
all_g0_FM = []

ga, da = run1.fe_calc_FM('sample_files/example1.wepy.h5', 500, file_idx=1)
all_d_FM.append(da)
all_g0_FM.append(ga)

ga2, da2 = run2.fe_calc_FM('sample_files/example2.wepy.h5', 500, file_idx=2)
all_d_FM.append(da2)
all_g0_FM.append(ga2)

# avgerage over both sets of data for the FM
mins_FM = []
for i in all_d_FM:
    mins_FM.append(len(i))
min_v_FM = np.amin(mins_FM)

d_short_FM = []
for i,v in enumerate(all_d_FM):
    d_short_FM.append(all_d_FM[i][0:min_v_FM])

g0_short_FM = []
for i,v in enumerate(all_g0_FM):
    g0_short_FM.append(all_g0_FM[i][0:min_v_FM])

d_mean_FM = np.mean(d_short_FM, axis=0)
g0_mean_FM = np.mean(g0_short_FM, axis=0)
print('Done with short method')

## make plots for the whole data set
# plots the AM and FM reults
plt.plot(r, target, color='k', linestyle='--', label='target')
plt.xlabel('Interatomic Distance (nm)')
plt.ylabel('Free Energy')
plt.plot(d_mean, g0_mean-g0_mean.min(), label='Avg, AM')
plt.plot(d_mean_FM, g0_mean_FM-g0_mean_FM.min(), label='Avg, FM')
plt.legend()
plt.savefig('all_data.pdf')

plt.close()

## make plots of the key data points
# plots the AM and FM results
plt.plot(r[0:125], target[0:125], color='k', linestyle='--', label='target')
plt.xlabel('Interatomic Distance (nm)')
plt.ylabel('Free Energy')
plt.plot(d_mean[0:27], (g0_mean-g0_mean.min())[0:27], label='Avg, AM')
plt.plot(d_mean_FM[0:27], (g0_mean_FM-g0_mean_FM.min())[0:27], label='Avg, FM')
plt.legend()
plt.savefig('key_data.pdf')

print('Figures saved: all_data.pdf & key_data.pdf')

