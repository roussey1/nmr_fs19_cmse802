## About this software

Enhanced sampling (ES) methods have been developed to study rare events in simulation. In the context of ES, rare events are events that without ES, would take a very long time to simulate or converge due to a long timescale of the event or due to the low probability of the event of interest. Many enhanced sampling methods have a long history, such as replica exchange, umbrella sampling, and metadynamics. While these methods are well developed, they are hard to use with systems that are out of equilibrium. There are different contexts for systems that are out of equilibrium, including systems that are naturally out of equilbrium, such as ion channels driven by a membrane potential, and systems that are intentionally driven out of equilbirium (in this case) doneso to exploit something called the Jarzynski Equality. The focus of my research is to develop enchanced sampling methods to study systems that are out of equilibrium, such as those mentioned above. Specifically, I will be developing tools to use the enhanced sampling method "Weighted Ensemble"(WE). The WE package utilized for the simulations that this package is designed for is '[Wepy]('https://github.com/ADicksonLab/wepy)'. 
   
   Free energy is a value that is of significant interest to both computational and experimental scientists. Free energy can be though of as a measure of the stability of a system. Free energies that are commonly of interest include protein (un)folding free energies, solvation free energies, and (un)binding free energies. (Un)binding free energies are of particular interest in drug development. For example, changes in this quantity are important when considering change that can be made to a drug candidate molecule. Due to the significance of free energies, new, effecient methods of accurately calculating free energies are always of interest. 
   
   Utilizing Wepy, I have developed a new method of calcuating (un)binding free energies from pulling trajectories. This method is still in the early stages of development. Currently, output from these simulations include work done on trajectories and probabilities. Using these values, we can obtain free energy surfaces for our systems of interest. These surfaces allow for the determination of free energy differences from different states (configurations) of our systems.
   
   The overall goal of this analysis program is to obtain a free energy surface from the data described above. This program is applicable only to simulations run with a specific build of the Wepy simualtion package. Sample data is provided and is from real simualtions of pulling trajectories done with a Lennard-Jones Pair. For more information about the theory behind this work and the use of computation in this field, please continue reading. 

## Additional theory
The theory for this project is based in theoretical chemistry (specifically nonequilibrium stastical mechanics), but the goal application is work with biomolecules and drug design. My main research topic focuses on studying properties on the Jarzynski Nonequilibrium Work Relation AKA '[The Jarzynski Equality](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.78.2690)'. This is a nonequilibrium statistical mechanics equation that relates the work done by a nonequilibrium process on a system to the equilibrium free energy difference between the inital and final states. This equation is of interest because it allows us to perform nonequilibrium work (along some system-relevant lambda from lambda_0 to lambda_1) to generate equilibrium free energy differences. The JE is as follows: <exp(-beta W)> = exp(-beta* Delta F), where <...> indicate the ensemble average, W is work, and Delta F is the Helmholtz free energy. This is significant because these nonequilibrium simualtions can be very fast and potentially less computationly expensive than traditional equilibrium simualtions used to determine the same value.
   
   The final free energy determined is dominated by low work trajectories, and we are interested in determining if enhancing the sampling of low work trajectories may allow us to find an accurate value of Delta F without the necessary number of trajectories otherwise needed for convergence. In other words, we want to develop a new method of calculating free energy differences between states of a system utilizing nonequilibrium processes and means of analyzing these nonequilibrium processes.
   
   One means of analysis of these nonequilibrium work processes is through the generation of free energy surfaces that can be used to find the value of Delta F.
   
   Computation is critical / a necessity in this field to run nonequilibrium simulations. Since the JE requires a very large number of realizations of a work process for accuracy, high performance computing is often required. Jarzynski has developed a fast switching method of simulation, thermodynamic integration, and biased path sampling methods have also been developed to study properties of the JE. Software such as '[OpenMM](http://openmm.org)' has prebuilt test systems including but not limited to a Lennard-Jones Pair that will be utilized in these simulations. 


## Authors

Nicole Roussey(1), Samuel Lotz(1), & Alex Dickson(1)(2)

(1). The Department of Biochemistry and Molecular Biology, Michigan State University, East Lansing, Michigan

(2). The Department of Computational Mathemetaics, Science, & Engineering, Michigan State University, East Lansing, Michigan


## Installation and Requirements

To use this software:

First clone the git repo.

#git clone https://gitlab.msu.edu/roussey1/nmr_fs19_cmse802.git

Installing the software is not necessary to run the example run_script. The software can be installed with pip. This may require updating pip to the latest version with "pip install --upgrade pip". The minimum python requirement is 3.6, however this can be modified in the setup.py file

#cd nmr_fs19_cmse802 

#pip install -e . 

Then install mdtraj with pip or conda forge via "conda install -c omnia mdtraj". On MacOS, if issues are happening with mdtraj installation, try "CFLAGS=-stdlib=libc++ pip install mdtraj". Next, install other requirements.

#pip install mdtraj

#pip install h5py

#pip install networkx

#conda install -c conda-forge matplotlib


## Example testing

Then to test the example script:

#python run_script.py

When done:

#open all_data.pdf

#open key_data.pdf

This example does analysis for real data from nonequilibrium pulling trajectories simulated using '[Wepy](https://github.com/ADicksonLab/wepy)' and '[OpenMM](http://openmm.org)'. The sample data is available in the "sample_files" folder. This example creates two graphs that are saved as PDFs.

## Tests
Tests are provided as such:

OK: Documented manual steps that can be followed to objectively check the expected functionality of the software (e.g., a sample input file to assert behavior)

Tests are provided in the "tests" folder. To run the first test, do "pytest test_target_plots.py". This test contains one test and takes approximately 1 second to run. This test determins if the target plot generator is creating output with the correct number of points for plotting. If this was not working correctly, plotting the target energy plot would not work. To run the second test, do "pytest test_binning_methods.py". This test contains two test and takes appoximately 15 seconds to run. This test determines if the analysis methods (the "binning" methods) are returning an equal number of bins and energies. If this is not working correctly, the functions are not doing what is expected and plotting the output will not work.

These tests also show proper input order and type for the functions in this software package.

## References

Software Packages

'[Wepy](https://github.com/ADicksonLab/wepy)'

'[OpenMM](http://openmm.org)'

'[mdtraj](http://mdtraj.org/1.9.3/)'

'[NumPy](https://numpy.org)'

'[NetworkX](https://networkx.github.io)'

'[h5py](https://www.h5py.org)'

'[matplotlib](https://www.h5py.org)'

'[Sphinx](https://www.sphinx-doc.org/en/master/index.html)'

'[Cookiecutter](https://cookiecutter.readthedocs.io/en/latest/)'

'[Pytest](https://docs.pytest.org/en/latest/)'

Papers

'[Nonequilibrium Equality for Free Energy Differences](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.78.2690)' Jarzynski, C., PRL, 1997

'[Free energy reconstruction from nonequilibrium single-molecule pulling experiments](https://www.pnas.org/content/98/7/3658)' Hummer, G., & Szabo, A., PNAS, 2001

A note: the files in weighted_histogram_method/wepy_files are not my work. They are from an older version of the ADicksonLab/Wepy project and are required to run the analysis written here.

## To see documentation

#cd /nmr_fs19_cmse802/docs/build/html

#open index.html